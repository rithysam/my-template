// @flow

import 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
Icon.loadFont();
import React, {useState, useEffect, useContext, useMemo, useReducer} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createDrawerNavigator } from "@react-navigation/drawer";


// Screens
import SignInScreen from './screens/auth/signInScreen';
import SignUpScreen from './screens/auth/signUpScreen';
import SplashScreen from './screens/splashScreen';
import AppInit from './AppInit';
import Home from './screens/Home';

import DrawerScreen from './components/drawer/DrawerScreen';

import AsyncStorage from '@react-native-community/async-storage';
import {stateConditionString} from './utils/helpers';
import {AuthContext} from './utils/authContext';
import {reducer, initialState} from './reducer';

const Stack = createStackNavigator();

function CustomDrawerContent(props){
  return (
    <DrawerScreen navigation={props.navigation}/>
  )
}
const Drawer = createDrawerNavigator();
const DrawerNavigator = () => (
  <Drawer.Navigator initialRouteName="StackNavigator" drawerContent={props => CustomDrawerContent(props)}>
    <Drawer.Screen name="StackNavigator" component={StackNavigator} />
  </Drawer.Navigator>
);


const HomeStack = createStackNavigator();
const StackNavigator = () => (
  <HomeStack.Navigator initialRouteName="Home" >
     
    <HomeStack.Screen 
       name="Home"
       component={Home}
      //  initialParams={{singOut: signOut}}
       options={{
         headerShown: false,
         title: '',
         headerStyle: {backgroundColor: 'black'},
         headerTintColor: 'white',
       }}
    />

    <HomeStack.Screen 
       name="AppInit"
       component={AppInit}
      //  initialParams={{singOut: signOut}}
       options={{
         headerShown: false,
         title: '',
         headerStyle: {backgroundColor: 'black'},
         headerTintColor: 'white',
       }}
    />
   
  </HomeStack.Navigator>
);

export default function App({ navigation }) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        // Restoring token failed
        console.log("error");
      }
      console.log(userToken);
     

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async data => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
    }),
    []
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.userToken == null ? (
            <Stack.Screen 
              name="SignIn" 
              component={SignInScreen} 
              options={
                {
                  headerShown: false
                }
              }
            />
          ) : (
            <Stack.Screen 
              name="Home" 
              component={DrawerNavigator} 
              options={{headerShown: false}}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

// export default App = ({navigation}) => {
//   const [state, dispatch] = useReducer(reducer, initialState);

//   useEffect(() => {
//     // Fetch the token from storage then navigate to our appropriate place
//     const bootstrapAsync = async () => {
//       let userToken;

//       try {
//         userToken = await AsyncStorage.getItem('userToken');
//       } catch (e) {
//         // Restoring token failed
//       }

      
//       if(userToken){
//         // console.log(userToken)
//         // console.log(state)
//         dispatch({type: 'HOME', token: userToken});
//       }
//       // After restoring token, we may need to validate it in production apps
//       // This will switch to the App screen or Auth screen and this loading
//       // screen will be unmounted and thrown away.
     
//     };
//     bootstrapAsync();
//   }, []);

//   // // In a production app, we need to send some data (usually username, password) to server and get a token
//   // // We will also need to handle errors if sign in failed
//   // // After getting token, we need to persist the token using `AsyncStorage`
//   const authContextValue = useMemo(
//     () => ({
//       signIn: async (data) => {
//         if (
//           data &&
//           data.emailAddress !== undefined &&
//           data.password !== undefined
//         ) {
//           dispatch({type: 'HOME', token: 'Token-For-Now'});
//         } else {
//           dispatch({type: 'TO_SIGNIN_PAGE'});
//         }
//       },
//       signOut: async (data) => {
//         dispatch({type: 'SIGN_OUT'});
//       },

//       signUp: async (data) => {
//         if (
//           data &&
//           data.emailAddress !== undefined &&
//           data.password !== undefined
//         ) {
//           dispatch({type: 'SIGNED_UP', token: 'dummy-auth-token'});
//         } else {
//           dispatch({type: 'TO_SIGNUP_PAGE'});
//         }
//       },
//     }),
//     [],
//   );



//   // bootstrap();

//   const chooseScreen =  (state) => {
    
//     // bootstrap();
//     let navigateTo = stateConditionString(state);
//     let arr = [];

//     console.log(navigateTo)

//     switch (navigateTo) {
//       case 'LOAD_APP':
//         arr.push(<Stack.Screen name="SplashScreen" component={SplashScreen} />);
//         break;

//       case 'LOAD_SIGNUP':
//         arr.push(
//           <Stack.Screen
//             name="SignUp"
//             component={SignUpScreen}
//             options={{
//               title: 'Sign Up',
//               animationTypeForReplace: state.isSignout ? 'pop' : 'push',
//             }}
//           />,
//         );
//         break;
//       case 'LOAD_SIGNIN':
//         arr.push(
//           <Stack.Screen 
//             name="SignIn" 
//             component={SignInScreen} 
//             options={{
//               headerShown: false,
//               title: '',
//               headerStyle: {backgroundColor: 'black'},
//               headerTintColor: 'white',
//             }}
//           />
//         );
//         break;

//       case 'LOAD_HOME':
//         arr.push(
//           <Stack.Screen
//             name="Home"
//             component={DrawerNavigator}
//             options={{
//               headerShown: false,
//               title: '',
//               headerStyle: {backgroundColor: 'black'},
//               headerTintColor: 'white',
//             }}
//           />,
//         );
//         break;
//       default:
//         arr.push(<Stack.Screen name="SignIn" component={SignInScreen} />);
//         break;
//     }
//     return arr[0];
//   };

  
//   return (
//     <AuthContext.Provider value={authContextValue}>
//       <NavigationContainer>
//         <Stack.Navigator>
//           {chooseScreen(state)}
//         </Stack.Navigator>
//       </NavigationContainer>
//     </AuthContext.Provider>
//   );
// };
