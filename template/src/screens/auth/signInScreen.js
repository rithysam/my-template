import React, { useEffect, useState, useContext, Component } from 'react';
import { validateAll } from 'indicative/validator';
import { View, Image } from 'react-native';
import { Container, Header, Content, Button, Item, Input, Thumbnail, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { AuthContext } from '../../utils/authContext';

// custom Component 
import {iconStyles, bgStyles, style, styles} from '../../components/styles';

const logo = '../../assets/img/logo.png';


const SignInScreen = ({ navigation }) => {
    const [emailAddress, setemailAddress] = useState('');
    const [password, setPassword] = useState('');
    const [SignUpErrors, setSignUpErrors] = useState({});

    const { signIn, signUp } = useContext(AuthContext);

    const handleSignIn = () => {
        // https://indicative.adonisjs.com
        const rules = {
            email: 'required|email',
            password: 'required|string|min:6|max:40'
        };

        const data = {
            email: emailAddress,
            password: password
        };

        const messages = {
            required: field => `${field} is required`,
            'username.alpha': 'Username contains unallowed characters',
            'email.email': 'Please enter a valid email address',
            'password.min': 'Wrong Password?'
        };

        validateAll(data, rules, messages)
            .then(() => {
                console.log('success sign in');
                signIn({ emailAddress, password });
            })
            .catch(err => {
                const formatError = {};
                err.forEach(err => {
                    formatError[err.field] = err.message;
                });
                setSignUpErrors(formatError);
            });
    };

    return (
        <Container>
            {/* <Header /> */}
            <Content style={[bgStyles.bacckground, styles.container]} >
                <View 
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 100,
                        marginBottom: 20
				    }}
                >
                    {/* <Thumbnail source={{uri:  require('../../assets/img/logo.png')}} /> */}
                    <Thumbnail square source={require('../../assets/img/logo.png')} style={{ flex: 1, width: 200, height: 200}}/>

                </View>
                <Item regular style={styles.input}>
                    <Icon name='user' style={iconStyles.iconSize}/>
                    <Input 
                        label={'Email'}
                        placeholder='Username'
                        value={emailAddress}
                        onChangeText={setemailAddress}
                        errorStyle={{ color: 'red' }}
                        errorMessage={SignUpErrors ? SignUpErrors.email : null}
                    />
                </Item>

                <Item regular style={styles.input}>
                    <Icon name='lock' style={iconStyles.iconSize}/>
                    <Input 
                        placeholder='Password'
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry
                        errorStyle={{ color: 'red' }}
                        errorMessage={SignUpErrors ? SignUpErrors.password : null}
                    />
                </Item>

                <Button 
                    block 
                    light 
                    style={bgStyles.bgWhite}
                    onPress={() => handleSignIn()}
                >
                    <Text style={styles.textPrimary}> LOGIN </Text>
                </Button>
            </Content>
        </Container>
    );
};

export default SignInScreen;
