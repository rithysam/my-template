import { StyleSheet } from 'react-native';

export const iconStyles = StyleSheet.create({
    iconSize: {
      fontSize: 30,
      padding: 10,
      color: '#ffffff'
    }
  });

export const bgStyles = StyleSheet.create({
    bacckground:{
        backgroundColor: '#65b0bf',
    },
    bgWhite:{
        backgroundColor: '#ffffff',
    },

    bgPrimary:{
        backgroundColor: '#0961ff',
    },

});

export const styles = StyleSheet.create({
    container:{
       padding: 5
    },
    input:{
        marginTop: 10,
        marginBottom: 10,
    },
    textWhite:{
        color: '#ffffff',
    },

    textPrimary:{
        color: '#0961ff',
    },
  

});